export const DISABLED = "disabled";
export const FULLSCREEN = "fullscreen";
export const VISIBLE = "visible";
export const HIDE = "hide";
export const LOADING = "loading";
export const HIDDEN = "hidden"
