export const rightBar = new Component(".bookContainer .toolBar .right");
export const lastBar = new Component(".bookContainer .toolBar .last");

export const book = new Component(".book");
export const bookContainer = new Component(".bookContainer .book");

export const leftArrow = new Component(".bookContainer .leftArrow");
export const rightArrow = new Component(".bookContainer .rightArrow");

export const frontBackgroundComp = new Component(this.selector + " .frontbackground");
export const backgroundComp = new Component(this.selector + " .background");

export const quitBtn = new Component(".bookContainer .toolBar .quitBtn");
export const fullscreenBtn = new Component(".bookContainer .fullScreenBtn");

export const firstPage = new Component(".firstPage");
export const slice = new Component(".bookContainer .toolBar .last");

//export const divImg = new Component(".bookContainer > img, .bookContainer > div");


