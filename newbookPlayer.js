import {DISABLED , FULLSCREEN, VISIBLE, HIDE}  from "./actionTypes";
//import {rightBar, lastBar} from "./components";
import * as eventFunctions from "./imagesEvents"

class Component {
    constructor(selector) {
        this.$el = document.querySelector(selector)
    }

    hide() {
        this.$el.style.display = 'none'
    }

    show() {
        this.$el.style.display = 'block'
    }

    addClass(type) {
        this.$el.classList.add(type)
    }

    removeClass(type) {
        this.$el.classList.remove(type)
    }

    animate({params}, speed) {
        this.$el.style.transition = 'all ' + speed;
        Object.keys(params).forEach((key) => 
            this.$el.style[key] = params[key]);
    }

    width() {
        return this.$el.offsetWidth;
    }

    height() {
        return this.$el.offsetHeigth;
    }

    css(attribute, value) {
        this.attribute = attribute;
        return this.$el.style.attribute = value;
    }
}

class BookPlayer extends Component {
    //replace this strings without static modifier
    //static ratio = 0;
    //static initialized = false;

    constructor (data, selector, readyCallback) {
        super(selector);
        this.selector = selector;
        this.$container = document.querySelector(selector);
        this.container = this.$container[0];
        this.data = data;

        this.container.ondragstart = function(){ return false;};
        this.container.ondrop = function(){ return false;};

        this.containerWidth = this.$container.width();
        this.containerHeight = this.$container.height();

        const wind = window.document.documentElement;
        wind.addEventListener('resize', (e) => {
            this.onWindowResize.bind(this)
        });
        
        const doc = document.documentElement;
        doc.addEventListener('webkitfullscreenchange', (e) => {
            this.onWindowResize.bind(this)
        });



    }

    // never used , but used public if   player.next()
    // ------
    next(index) {
        this.$book.turn('page', index);
    };

    onWindowResize() {
        const size =  this.resize();
        if (!size) return;

		const _isFullScreen = isFullScreen();
		
        if (!_isFullScreen)
            size = this.size;
        
		 this.fullSreenSize(_isFullScreen);

        this.$book.turn('resize');
        this.$book.turn('size', size.width, size.height);

        this.updateElements(true);

        eventFunctions.updateSlice.call(this, this.$book.turn('page'));
    }

    resize() {
        
        const container = new Component(".bookContainer");

        if (!this.$book) return;
        // reset the width and height to the css defaults
        this.$book[0].style.width = '';
        this.$book[0].style.height = '';


        let widthPercent = 0.90;
        if (!isFullScreen())
            widthPercent = 0.85;
        
        const maxWidth = Math.round(container[0].clientWidth * widthPercent);
        const maxHeight = Math.round(container[0].clientHeight * widthPercent);

		
		let width, height;
		
		if (maxWidth / this.ratio / 2 > maxHeight)
		{
			height = maxHeight;
            width = Math.round(maxHeight * this.ratio * 2);
		}
		else
		{
			width = maxWidth;
            height = width / this.ratio / 2;
		}
		
        // set the width and height matching the aspect ratio
        this.$book[0].style.width = width + 'px';
        this.$book[0].style.height = height + 'px';

        return {
            width: width,
            height: height
        };
    }


    // in original method here 3 parametrs, but 2 of them never used
    // i dont know how implement them correctly (???)

    updateElements(fast) { 
        
        const bookContainer = new Component(".bookContainer");
        const leftArrow = new Component(".bookContainer .leftArrow");
        const rightArrow = new Component(".bookContainer .rightArrow");

        this.$book.css({top:Math.round((bookContainer.height() - this.$book.height()) / 2)});

        if (!this.$book.turn('page')) return;

        let duration = 150;
        if (fast === true || !this.initialized)
            duration = 0;

        if (this.$book.turn('page') == 1)
        {
            this.$book.animate({left:Math.round(((bookContainer.width() - (this.$book.width()/2 )) / 2) - (this.$book.width()/2))}, duration, this.positionBackground.bind(this));
            rightArrow.animate({"left": Math.round(((bookContainer.width() - (this.$book.width()/2 )) / 2) + (this.$book.width()/2))}, duration);
        }
        else
        if ( this.$book.turn('page') == this.$book.turn('pages'))
        {
            this.$book.animate({left:Math.round(((bookContainer.width() - (this.$book.width()/2 )) / 2)) }, duration, this.positionBackground.bind(this));
            leftArrow.animate({left:Math.round(((bookContainer.width() - (this.$book.width()/2 )) / 2)  - leftArrow.width()) + (isFullScreen() ? 0 : 2) }, duration);

        }
        else
        {
            let ratio = 23/1421;
            if (isFullScreen())
                ratio = 31/1421;

            this.$book.animate({left:Math.round((bookContainer.width() - this.$book.width() ) / 2)}, duration, this.positionBackground.bind(this));
            rightArrow.animate({"left": Math.round(((bookContainer.width() - this.$book.width() ) / 2)  + (this.$book.width()) + this.$book.width()*ratio)}, duration) ;
            leftArrow.animate({left:Math.round((bookContainer.width() - this.$book.width() ) / 2 - leftArrow.width() - this.$book.width()*ratio) + (isFullScreen() ? 1 : 0) }, duration);

        }

        // buttons
        if (this.$book.turn('page')==1)
        {
            leftArrow.hide();           
        }
        else
        {
            leftArrow.show();            
        }
                
        const rightBar = new Component(".bookContainer .toolBar .right");
        const lastBar = new Component(".bookContainer .toolBar .last");

        if (this.$book.turn('page')== this.$book.turn('pages'))
        {
            rightArrow.hide();
            rightBar.addClass(DISABLED);
            lastBar.addClass(DISABLED);
        }
        else
        {
            rightArrow.show();
            rightBar.removeClass(DISABLED);
            lastBar.removeClass(DISABLED);
        }

    }

    positionBackground()
    {
        const bookComponent = new Component(".bookContainer .book");
        const frontBackgroundComp = new Component(this.selector + " .frontbackground");
        const backgroundComp = new Component(this.selector + " .background");
        const rightArrow = new Component (".bookContainer .rightArrow");
        const leftArrow = new Component (".bookContainer .leftArrow");
        const fullScreenBtn = new Component (".bookContainer .fullScreenBtn");
        const bookContainer = new Component (".bookContainer");
        
        if (this.$book.turn('page') == 1)
        {
            frontBackgroundComp.css({width : Math.round(this.$book.width() / 2), height : Math.round(this.$book.height()), top : Math.round(this.$book.position().top), left : Math.round(this.$book.position().left + this.$book.width() / 2)});

            backgroundComp.hide();
            frontBackgroundComp.show();
            bookComponent.css("border", "");
        }
        else
        if ( this.$book.turn('page') == this.$book.turn('pages'))
        {
            frontBackgroundComp.css({width : Math.round(this.$book.width() / 2), height : Math.round(this.$book.height()), top : Math.round(this.$book.position().top), left : Math.round(this.$book.position().left) });

            backgroundComp.hide();
            frontBackgroundComp.show();
            bookComponent.css("border", "");
        }
        else
        {
            backgroundComp.css({ width: Math.round(( this.$book.width() * 1486/1420)) + "px",
                height :Math.round( this.$book.height() * 759/739) + "px",
                top : Math.round(this.$book.position().top -  this.$book.height()*6/740),
                left : Math.round(this.$book.position().left - this.$book.width()*32/1421)});

            backgroundComp.show();
            frontBackgroundComp.hide();
            bookComponent.css("border", "solid 1px rgba(0,0,0,0.05)");
        }
        


        rightArrow.css({"top": Math.round(this.$book.position().top + this.$book.height() / 2 - rightArrow.height() / 2)}) ;
        leftArrow.css({"top": Math.round(this.$book.position().top + this.$book.height() / 2 - rightArrow.height() / 2)}) ;

        fullScreenBtn.css({left: Math.round((bookContainer.width() - fullScreenBtn.width()) / 2),
                                                top: Math.round((bookContainer.height() - fullScreenBtn.height()) / 2) });

        updateSlice.call(this,  this.$book.turn('page') );
    }

    fullSreenSize (value) {
        
        const selector = new Component(".bookContainer .toolBar");
        const fullScreenButton = new Component(".bookContainer .fullScreenBtn");
        
        if (value)
        {
            this.$container.addClass(FULLSCREEN);

            this.container.style.width = "100%";
            this.container.style.height = "100%";
			this.container.style.top = "0px";
			this.container.style.left = "0px";
            
            fullScreenButton.removeClass(VISIBLE);
            selector.show();
            
        }
        else
        {
            this.$container.removeClass(FULLSCREEN);

            this.container.style.width = this.containerWidth + "px";
            this.container.style.height = this.containerHeight + "px";
			this.container.style.top = "";
			this.container.style.left = "";
            this.container.style.backgroundColor = "";

            selector.hide();
        }
    }

    build (readyCallback) {

        this.hasSlice = this.data.hasSlice;

        let html = '';

        if (this.hasSlice)
            html += `<img class="slice" style="position:absolute;" src=${this.data.slice}>
                        <img class="background" src="/images/bookplayer/background-shape.png">
                        <div class="frontbackground" ></div>
                        <div class="book">`;

        const data = this.data;

        data.forEach((pages, index) => {
		
            if (index == 1)
                html += `<div class="page hard" style="background-color:white"><img src="/images/bookplayer/left-empty.jpg" style="width:100%;height:100%"></div>
                            <div class="page`;

            if (index == 1 || index == pages.length-1)
                html += ` hard'">
                        <img class="image `;

            if (index == 0)
                html += ` firstPage'" style="position:absolute;width:100%;height:100%" >`;
         
            if (index > 0 && index < pages.length-1)
            {
                if (index %2)
                    html += `<img src="/images/bookplayer/rightPage.png" style="position:absolute;width:100%;height:100%">`
                else
                    html += `<img src="/images/bookplayer/leftPage.png" style="position:absolute;width:100%;height:100%">`
            }

            html += `</div>`;

            if ( index == pages.length-2)
                html += `<div class="page hard" style="background-color:white"><img src="/images/bookplayer/right-empty.jpg" style="width:100%;height:100%"></div>`;
        });

        html += `</div>

        <svg class="icon leftArrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 91.5 183" xml:space="preserve">
        <path class="st0" d="M91.5,183C41,183,0,142,0,91.5C0,41,41,0,91.5,0"/>
        <path class="st1" d="M64,58L36.1,85.9c-1.7,1.7-1.7,4.5,0,6.2L64,120"/>
        </svg>

        <svg class="icon rightArrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 91.5 183" xml:space="preserve">
        <path class="st0" d="M0,183c50.5,0,91.5-41,91.5-91.5S50.5,0,0,0"/>
        <path class="st1" d="M27.5,58l27.9,27.9c1.7,1.7,1.7,4.5,0,6.2L27.5,120"/>
        </svg>

        <div class="toolBar">
            <div class="quitBtn">Quitter</div>
        </div>
        <div class="fullScreenBtn" >Cliquez ici pour agrandir</div>`;

        this.$container.append(html);

            // need rework this part of method !!!
            // --------------------------------------------------------------
        const divImg = new Component(".bookContainer > img, .bookContainer > div");
        divImg.addClass(HIDE);
        
        const firstPage = new Component(".firstPage");
        firstPage.addEventListener('load', (e) => {
            eventFunctions.onFirstPageLoaded.bind(this)
        });
		
        this.$book = document.querySelector(".book");

        const leftArrow = new Component(".bookContainer .leftArrow");
        leftArrow.addEventListener('click', (e) => {
            eventFunctions.onPrevious.bind(this)
        });

        const rightArrow = new Component(".bookContainer .rightArrow");
        rightArrow.addEventListener('click', (e) => {
            eventFunctions.onNext.bind(this)
        });

        const fullscrnBtn = new Component(".bookContainer .fullScreenBtn");
        fullscrnBtn.addEventListener('click', (e) => {
            eventFunctions.onFullScreen.bind(this)
        });
       
        const quitBtn = new Component(".bookContainer .toolBar .quitBtn");
        quitBtn.addEventListener('click', (e) => this.exitFullScreenMode.bind(this) )

            // need rework this part of method !!!
            // --------------------------------------------------------------
        $(".bookContainer .book, .bookContainer .fullScreenBtn").on("mouseover", onMouseOverBook.bind(this));
        $(".bookContainer .book").on("mouseout", onMouseOutBook.bind(this));
            // -----------------------------------------------------------------
            // -----------------------------------------------------------------
        this.ratio = this.data.pageWidth / this.data.pageHeight;

        this.size = this.resize();


        this.$book.turn({       gradients: true,
                                acceleration: !isChrome(),
                                pages: this.data.pages.length,
                                when: {
                                    turning : this.preUpdateElements.bind(this),
                                    turned: this.updateElements.bind(this)
                        }});

                        eventFunctions.loadImage.call(this, 1, 0);
			
		this.$book.css("position", "absolute");

        this.updateElements(false);

        eventFunctions.updateSlice.call(this, 1);

        this.ready = true;

        if (readyCallback)
            readyCallback();

    }


    // --------------------------- Fullscreen -------------------------------- //

    onFullScreen() {
        (!isFullScreen()) ? this.enterFullScreen() : this.exitFullScreenMode();
    }

    enterFullScreen() {
        fullScreen = true;
		
        const element = this.container;

        if(element.requestFullscreen) {
            element.requestFullscreen();
        } else if(element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if(element.webkitRequestFullScreen) {
            element.webkitRequestFullScreen();
        } else if(element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
		
		// because on safari, fullScreen is detected only some time after the switch
        setTimeout(onEnterFullScreen.bind(this), 1000);
    }

    exitFullScreenMode() {
        if(document.exitFullscreen) {
            document.exitFullscreen();
        } else if(document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if(document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if(document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if(document.msExitFullscreen) {
            document.msExitFullscreen();
		} else if(document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }

    
}