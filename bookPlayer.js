(function(){

    window.BookPlayer = function()
    {
        this.ratio = 1;
		this.initialized = false;
    };

    BookPlayer.prototype.init = function(data, bookid, selector, readyCallback)
    {
        this.selector = selector;
        this.$container = $(selector);
        this.container = this.$container[0];
        this.data = data;

        this.container.ondragstart = function(){ return false;};
        this.container.ondrop = function(){ return false;};

        this.containerWidth = this.$container.width();
        this.containerHeight = this.$container.height();

        this.build(readyCallback);
        

        $(window).on('resize', this.onWindowResize.bind(this));
        $(document).on('webkitfullscreenchange', this.onWindowResize.bind(this));
        //$(document).on('mozfullscreenchange', function(){console.log("fullscreenchange"); this.onWindowResize();} );
        
        
    };

    BookPlayer.prototype.next = function(index) {
        this.$book.turn('page', index); 
    };

    BookPlayer.prototype.onWindowResize = function () {
        var size =  this.resize();
        if (!size) return;

		var _isFullScreen = isFullScreen();
		
        if (!_isFullScreen)
            size = this.size;
        
		 this.fullSreenSize(_isFullScreen);

        this.$book.turn('resize');
        this.$book.turn('size', size.width, size.height);

        this.updateElements(true);

        updateSlice.call(this, this.$book.turn('page'));
    };

    BookPlayer.prototype.updateElements = function(fast, index, indexes)
    {
        this.$book.css({top:Math.round(($(".bookContainer").height() - this.$book.height()) / 2)});

        if (!this.$book.turn('page')) return;

        var duration = 150;
        if (fast === true || !this.initialized)
            duration = 0;

        if (this.$book.turn('page') == 1)
        {
            this.$book.animate({left:Math.round((($(".bookContainer").width() - (this.$book.width()/2 )) / 2) - (this.$book.width()/2))}, duration, this.positionBackground.bind(this));

            $(".bookContainer .rightArrow").animate({"left": Math.round((($(".bookContainer").width() - (this.$book.width()/2 )) / 2) + (this.$book.width()/2))}, duration);


        }
        else
        if ( this.$book.turn('page') == this.$book.turn('pages'))
        {
            this.$book.animate({left:Math.round((($(".bookContainer").width() - (this.$book.width()/2 )) / 2)) }, duration, this.positionBackground.bind(this));

            $(".bookContainer .leftArrow").animate({left:Math.round((($(".bookContainer").width() - (this.$book.width()/2 )) / 2)  - $(".bookContainer .leftArrow").width()) + (isFullScreen() ? 0 : 2) }, duration);

        }
        else
        {
            var ratio = 23/1421;
            if (isFullScreen())
                ratio = 31/1421;

            this.$book.animate({left:Math.round(($(".bookContainer").width() - this.$book.width() ) / 2)}, duration, this.positionBackground.bind(this));
            $(".bookContainer .rightArrow").animate({"left": Math.round((($(".bookContainer").width() - this.$book.width() ) / 2)  + (this.$book.width()) + this.$book.width()*ratio)}, duration) ;
            $(".bookContainer .leftArrow").animate({left:Math.round(($(".bookContainer").width() - this.$book.width() ) / 2 - $(".bookContainer .leftArrow").width() - this.$book.width()*ratio) + (isFullScreen() ? 1 : 0) }, duration);

        }

        // buttons
        if (this.$book.turn('page')==1)
        {
            $(".bookContainer .leftArrow").hide();           
        }
        else
        {
            $(".bookContainer .leftArrow").show();            
        }

        if (this.$book.turn('page')== this.$book.turn('pages'))
        {
            $(".bookContainer .rightArrow").hide();
            $(".bookContainer .toolBar .right").addClass("disabled");
            $(".bookContainer .toolBar .last").addClass("disabled");
        }
        else
        {
            $(".bookContainer .rightArrow").show();
            $(".bookContainer .toolBar .right").removeClass("disabled");
            $(".bookContainer .toolBar .last").removeClass("disabled");
        }
      
    };

    BookPlayer.prototype.positionBackground = function()
    {
        if (this.$book.turn('page') == 1)
        {
            $(this.selector + " .frontbackground").css({width : Math.round(this.$book.width() / 2), height : Math.round(this.$book.height()), top : Math.round(this.$book.position().top), left : Math.round(this.$book.position().left + this.$book.width() / 2)});

            $(this.selector + " .background").hide();
            $(this.selector + " .frontbackground").show();
            $(".bookContainer .book").css("border", "");
        }
        else
        if ( this.$book.turn('page') == this.$book.turn('pages'))
        {
            $(this.selector + " .frontbackground").css({width : Math.round(this.$book.width() / 2), height : Math.round(this.$book.height()), top : Math.round(this.$book.position().top), left : Math.round(this.$book.position().left) });

            $(this.selector + " .background").hide();
            $(this.selector + " .frontbackground").show();
            $(".bookContainer .book").css("border", "");
        }
        else
        {
            $(this.selector + " .background").css({ width: Math.round(( this.$book.width() * 1486/1420)) + "px",
                height :Math.round( this.$book.height() * 759/739) + "px",
                top : Math.round(this.$book.position().top -  this.$book.height()*6/740),
                left : Math.round(this.$book.position().left - this.$book.width()*32/1421)});

            $(this.selector + " .background").show();
            $(this.selector + " .frontbackground").hide();
            $(".bookContainer .book").css("border", "solid 1px rgba(0,0,0,0.05)");


        }
        
        $(".bookContainer .rightArrow").css({"top": Math.round(this.$book.position().top + this.$book.height() / 2 - $(".bookContainer .rightArrow").height() / 2)}) ;
        $(".bookContainer .leftArrow").css({"top": Math.round(this.$book.position().top + this.$book.height() / 2 - $(".bookContainer .rightArrow").height() / 2)}) ;


       
         $(".bookContainer .fullScreenBtn").css({left: Math.round(($(".bookContainer").width() - $(".bookContainer .fullScreenBtn").width()) / 2),
                                                top: Math.round(($(".bookContainer").height() - $(".bookContainer .fullScreenBtn").height()) / 2) });

        updateSlice.call(this,  this.$book.turn('page') );
    }

    BookPlayer.prototype.resize = function()
    {
	
        if (!this.$book) return;
        // reset the width and height to the css defaults
        this.$book[0].style.width = '';
        this.$book[0].style.height = '';


        var widthPercent = 0.90;
        if (!isFullScreen())
            widthPercent = 0.85;
        
        var maxWidth = Math.round($(".bookContainer")[0].clientWidth * widthPercent);
        var maxHeight = Math.round($(".bookContainer")[0].clientHeight * widthPercent);

		
		var width, height;
		
		if (maxWidth / this.ratio / 2 > maxHeight)
		{
			height = maxHeight;
            width = Math.round(maxHeight * this.ratio * 2);
		}
		else
		{
			width = maxWidth;
            height = width / this.ratio / 2;
		}
		
        // set the width and height matching the aspect ratio
        this.$book[0].style.width = width + 'px';
        this.$book[0].style.height = height + 'px';


        return {
            width: width,
            height: height
        };
    };

    BookPlayer.prototype.resizeContainer = function()
    {
        /*if (!this.isFullScreen())
            this.fullSreenSize(false);*/

        //this.updateElements(true);

    };




    BookPlayer.prototype.fullSreenSize = function(value)
    {
		if (value)
        {
            this.$container.addClass("fullscreen");

            this.container.style.width = "100%";
            this.container.style.height = "100%";
			this.container.style.top = "0px";
			this.container.style.left = "0px";
            
            $(".bookContainer .fullScreenBtn").removeClass("visible");
            $(".bookContainer .toolBar").show();
        }
        else
        {
            this.$container.removeClass("fullscreen");

            this.container.style.width = this.containerWidth + "px";
            this.container.style.height = this.containerHeight + "px";
			this.container.style.top = "";
			this.container.style.left = "";
            this.container.style.backgroundColor = "";

            $(".bookContainer .toolBar").hide();
        }

    };

    BookPlayer.prototype.build = function(readyCallback)
    {   
        this.hasSlice = this.data.hasSlice;

        var html = '';

        if (this.hasSlice)
            html += '<img class="slice" style="position:absolute;" src="' + this.data.slice + '">';

        html += '<img class="background" src="/images/bookplayer/background-shape.png">';
        html += '<div class="frontbackground" ></div>';
        html += '<div class="book">';

        var data = this.data;
        $.each(this.data.pages, function(index){
		
            if (index == 1)
                html += '<div class="page hard" style="background-color:white"><img src="/images/bookplayer/left-empty.jpg" style="width:100%;height:100%"></div>';

            html += '<div class="page';

            if (index == 1 || index == data.pages.length-1)
                html += ' hard';

            html += '"><img class="image ';

            if (index == 0)
                html += ' firstPage';

            html += '" style="position:absolute;width:100%;height:100%" >';
         
            if (index > 0 && index < data.pages.length-1)
            {
                if (index %2)
                    html += '<img src="/images/bookplayer/rightPage.png" style="position:absolute;width:100%;height:100%">'
                else
                    html += '<img src="/images/bookplayer/leftPage.png" style="position:absolute;width:100%;height:100%">'
            }

            html += '</div>';

            if ( index == data.pages.length-2)
                html += '<div class="page hard" style="background-color:white"><img src="/images/bookplayer/right-empty.jpg" style="width:100%;height:100%"></div>';
        });

        html += '</div>';
   

        html += '<svg class="icon leftArrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 91.5 183" xml:space="preserve">';
        html += '<path class="st0" d="M91.5,183C41,183,0,142,0,91.5C0,41,41,0,91.5,0"/>';
        html += '<path class="st1" d="M64,58L36.1,85.9c-1.7,1.7-1.7,4.5,0,6.2L64,120"/>';
        html += '</svg>';

        html += '<svg class="icon rightArrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 91.5 183" xml:space="preserve">';
        html += '<path class="st0" d="M0,183c50.5,0,91.5-41,91.5-91.5S50.5,0,0,0"/>';
        html += '<path class="st1" d="M27.5,58l27.9,27.9c1.7,1.7,1.7,4.5,0,6.2L27.5,120"/>';
        html += '</svg>';

        html += '<div class="toolBar">';
        html += '<div class="quitBtn">';
        
        html += 'Quitter';
        html += '</div>';
        html += '</div>';
        html += '<div class="fullScreenBtn" >Cliquez ici pour agrandir</div>';

        this.$container.append(html);

        
        $(".bookContainer > img, .bookContainer > div").addClass("hide");
		
		$(".firstPage").on('load', onFirstPageLoaded.bind(this))

        this.$book = $(".book");

        $(".bookContainer .leftArrow").on("click", onPrevious.bind(this) );
        $(".bookContainer .rightArrow").on("click", onNext.bind(this) );
        $(".bookContainer .fullScreenBtn").on("click", this.onFullScreen.bind(this));

        $(".bookContainer .toolBar .quitBtn").on("click", this.exitFullScreenMode.bind(this));

        $(".bookContainer .book, .bookContainer .fullScreenBtn").on("mouseover", onMouseOverBook.bind(this));
        $(".bookContainer .book").on("mouseout", onMouseOutBook.bind(this));


        this.ratio = this.data.pageWidth / this.data.pageHeight;

        this.size = this.resize();


        this.$book.turn({     gradients: true,
                                acceleration: !isChrome(),
                                pages: this.data.pages.length,
                                when: {
                                    turning : this.preUpdateElements.bind(this),
                                    turned: this.updateElements.bind(this)
                        }});

        loadImage.call(this, 1, 0);
			
		this.$book.css("position", "absolute");

        this.updateElements(false);

        updateSlice.call(this, 1);

        this.ready = true;

        if (readyCallback)
            readyCallback();
      
    };

    function loadAllImages()
    {
         var nbpages = this.$book.turn("pages");
         for (var i = 3; i <= nbpages; i++)
         {
            if (i == this.data.pages.length + 2)
                loadImage.call(this, i, i - 3);
            else
                loadImage.call(this, i, i - 2);
         }
            

         this.loaded = true;
    }
    function loadImage(indexBook, indexPages)
    {
        var $image = this.$container.find(".page-wrapper[page=" + indexBook + "] .image");
        if ($image.attr("src") || $image.hasClass("loading"))
            return;

        $image.addClass("loading");

        var img = new Image();
        img.onload = onImageLoaded.bind(this, $image, img);
        img.src = this.data.pages[indexPages];

    }

    function onImageLoaded($image, img){
        $image.attr("src", img.src);
        $image.removeClass("loading");
    };

    function onFirstPageLoaded()
    {
        this.initialized = true; 
        $(".bookContainer .rightArrow").removeClass("hidden");
        this.updateElements(true);
    }

    function onPrevious(){ 
        this.$book.turn('previous'); 
    }

    function onNext() { 
        this.$book.turn('next'); 

        if ( !this.loaded)
            loadAllImages.call(this);
    }

    function onGoFirstPage(){
        this.$book.turn("page", 1);
    }

    function onGoLeftPage(){ 
        this.$book.turn('previous'); 
    }

    function onGoRightPage(){ 
        this.$book.turn('next'); 
    }

    function onGoLastPage(){
        this.$book.turn("page", this.data.pages.length + 2)
    }

    function onMouseOverBook(){ 
        if (!isFullScreen() && !isMobileOrTablet()) 
            $(".bookContainer .fullScreenBtn").addClass("visible");
    }

    function onMouseOutBook(){ 
        $(".bookContainer .fullScreenBtn").removeClass("visible");
    }

    function updateSlice(index)
    {
        if (!this.hasSlice || (index > 1 && index != this.$book.turn('pages')))
        {
            $(".slice").hide();
        }
        else
        {
            $(".slice").height(  $(".book").height() );
            var positon = $(".frontbackground").position();

            var offset = 5;
            if (isFullScreen())
                offset = 10;

            if (index == 1) {
                $(".slice").css({top: positon.top, left: positon.left - $(".slice").width() - offset});
                $(".slice").show();
            }
            else {
                $(".slice").css({top: positon.top, left: positon.left + $(".frontbackground").width() + offset});
                $(".slice").hide();
            }

        }
    }

    BookPlayer.prototype.preUpdateElements = function(event, index, indexes)
    {
        if (index == 1 || index == this.$book.turn('pages'))
            $(this.selector + " .background").hide();
        else
            $(this.selector + " .frontbackground").hide();

        if (indexes)
        {
            if (index == 1)
                loadImage.call(this, 1, 0);
            else
            if (index == 2)
                loadImage.call(this, 3, 1);
            else
            if (index == this.data.pages.length + 2)
                loadImage.call(this, index, indexes[0] - 3);
            else
            {
                loadImage.call(this, index, indexes[0] - 2);
                loadImage.call(this, index+1, indexes[1] - 2);
            }
            
        }



    }

    // --------------------------- Fullscreen -------------------------------- //

    BookPlayer.prototype.onFullScreen = function()
    {
        if (!isFullScreen())
            this.enterFullScreen();
        else
            this.exitFullScreenMode();

    }

    BookPlayer.prototype.enterFullScreen = function()
    {
		fullScreen = true;
		
        var element = this.container;
        if(element.requestFullscreen) {
            element.requestFullscreen();
        } else if(element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if(element.webkitRequestFullScreen) {
            element.webkitRequestFullScreen();
        } else if(element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
		
		// because on safari, fullScreen is detected only some time after the switch
        setTimeout(onEnterFullScreen.bind(this), 1000);
    }

    function onEnterFullScreen(){ 
        this.onWindowResize();
    }

    BookPlayer.prototype.exitFullScreenMode = function() {
	    if(document.exitFullscreen) {
            document.exitFullscreen();
        } else if(document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if(document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if(document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if(document.msExitFullscreen) {
            document.msExitFullscreen();
		} else if(document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }

        //setTimeout(function(){ this.onWindowResize();}, 250);
    }


    function isFullScreen()
    {
        return  (document.fullScreen || document.mozFullScreen || document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.webkitIsFullScreen || document.webkitCurrentFullScreenElement || document.msFullscreenElement ) != undefined;
    }

    function isChrome() {

        return navigator.userAgent.indexOf('Chrome')!=-1;

    }
	
	function isMobileOrTablet(){
	  var check = false;
	  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
	  return check;
	}

})();
