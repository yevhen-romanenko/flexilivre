import {VISIBLE, LOADING, HIDDEN}  from "./actionTypes";

const loadAllImages = function(){
    const nbpages = this.$book.turn("pages");
    //let i = 3;??
    for (let i of nbpages)
    {
       if (i == this.data.pages.length + 2)
           loadImage.call(this, i, i - 3);
       else
           loadImage.call(this, i, i - 2);
    }
    
    this.loaded = true;
};

const loadImage = function(indexBook, indexPages) {
    const $image = this.$container.find(`.page-wrapper[page=${indexBook}] .image`);
    if ($image.attr("src") || $image.hasClass(LOADING))
        return;
    
        $image.addClass(LOADING);

    const img = new Image();
    img.onload = onImageLoaded.bind (this, $image, img);
    img.src = this.data.pages[indexPages];
}

const onImageLoaded = function ($image, img){
    $image.attr("src", img.src);
    $image.removeClass(LOADING);
};

const onFirstPageLoaded = function(){
    const rightArrow = new Component(".bookContainer .rightArrow");

    this.initialized = true; 
    rightArrow.removeClass(HIDDEN);
    this.updateElements(true);

}

const onPrevious = function() {
    this.$book.turn('previous');
}

const onNext = function() {
    this.$book.turn('next');
    
    if (!this.loaded)
            loadAllImages.call(this);
}

const onGoFirstPage = function() {
    this.$book.turn("page", 1);
}

const onGoLeftPage = function() {
    this.$book.turn('previous');
}

const onGoRightPage = function() {
    this.$book.turn('next');
}

const onGoLastPage = function() {
    this.$book.turn("page", this.data.pages.length + 2);
}

const onMouseOverBook = function() {
    const fullScreenBtn = new Component(".bookContainer .fullScreenBtn");
    if (!isFullScreen() && !isMobileOrTablet()) 
        fullScreenBtn.addClass(VISIBLE);
}

const onMouseOutBook = function() {
    const fullScreenBtn = new Component(".bookContainer .fullScreenBtn");
    fullScreenBtn.removeClass(VISIBLE);
}

const updateSlice = function(index) {
    const slice = new Component(".slice");
    
    if (!this.hasSlice || (index > 1 && index != this.$book.turn('pages')))
    {
        slice.hide();
    }
    else
    {
        const book = new Component(".book");
        slice.height( book.height() );
        const frontbackground = new Component(".frontbackground");
        const position = frontbackground.position();

        let offset = 5;
        if (isFullScreen())
            offset = 10;

        if (index == 1) {
            slice.css({top: position.top, left: position.left - slice.width() - offset});
            slice.show();
        }
        else {
            slice.css({top: position.top, left: position.left + frontbackground.width() + offset});
            slice.hide();
        }

    }    
}

export default {
    loadAllImages,
    loadImage,
    onImageLoaded,
    onFirstPageLoaded,
    onGoFirstPage,
    onGoLastPage,
    onGoLeftPage,
    onGoRightPage,
    onMouseOutBook,
    onMouseOverBook,
    onPrevious, 
    onNext,
    updateSlice
};
    



   

   